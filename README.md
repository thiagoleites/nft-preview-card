# NFT Preview Card component

## Solution for the challenge of the [Frontend Mentor](https://www.frontendmentor.io)

### Using the frontend
 - HTML
 - CSS
 - Stylus pre-processing

### The solution can be found in [here](https://thiero.com.br/nft-challenge)